import { CarModel } from "../enums/carModel";

export interface IUser {
  _id: string;
  email: string;
  password: string;
  nickname: string;
  avgCPM: number;
  totalRaceAmount: number;
  wonRaceAmount: number;
  role: string;
  cars: Array<CarModel>;
  balance: number; 
  rating: number;
}