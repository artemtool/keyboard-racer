export interface IArticle {
  _id: string;
  text: string;
}