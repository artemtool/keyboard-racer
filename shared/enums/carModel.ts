export enum CarModel {
  Audi_TT = 0,
  Chevrolet_Corvete = 1,
  Chevrolet_Malibu = 2,
  Lamborghini_Huracan = 3,
  Nissan_GTR = 4
}