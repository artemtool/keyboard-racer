import * as React from 'react';

interface Props {
  onSubmit: (members: number) => void;
}

interface State {
  members: number;
}

export default class CreateRaceForm extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      members: 2
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  public render() {
    return (
      <form
        className="create-race-form"
        onSubmit={this.onSubmit}
        noValidate
      >
        <label htmlFor="members">Members amount:</label>
        <div className="input-with-button">
          <input
            id="members"
            name="members"
            type="number"
            min="2"
            max="5"
            value={this.state.members}
            onChange={this.onValueChange}
          />
          <button type="submit">Create private race</button>
        </div>
      </form>
    );
  }

  private onValueChange(e: any) {
    e.persist();

    this.setState(state => ({
      ...state,
      [e.target.name]: Number(e.target.value)
    }));
  }

  private onSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();
    this.props.onSubmit(this.state.members);
  }
}