import './RacePath.scss';
import * as React from 'react';
import { Car } from './../../models/car';

interface Props {
  progress: number;
  nickname: string;
  CPM: number;
  car: Car;
  place: number;
}

interface State {
  carWidth: number;
}

export default class RacePath extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      carWidth: 0
    }

    this.onCarLoad = this.onCarLoad.bind(this);
  }

  public render() {
    const carPosition = this.props.progress + '%';
    return (
      <div className="race-path-container">
        <span className="race-path-container__nickname">@{this.props.nickname}</span>
        <div
          className="race-path-container__race-path"
          style={{ marginRight: this.state.carWidth }}
        >
          <img
            src={this.props.car.imageUrl}
            alt={this.props.car.label}
            style={{ marginLeft: carPosition }}
            className="race-path-container__race-car"
            onLoad={this.onCarLoad}
          />
        </div>
        <div className="race-path-container__result">
          <span>{this.props.CPM} CPM</span>
          {
            this.props.progress === 100
              ? (
                <React.Fragment>
                  <img src="/public/assets/images/finish-flag.png" alt="Finished" />
                  <span>{this.props.place} st</span>
                </React.Fragment>
              )
              : (
                <React.Fragment>
                  <img src="/public/assets/images/steering-wheel.png" alt="Steering wheel" />
                  <span>{this.props.progress} / 100</span>
                </React.Fragment>
              )
          }
        </div>
      </div>
    );
  }

  private onCarLoad(e: React.SyntheticEvent<HTMLImageElement>) {
    this.setState({
      carWidth: (e.target as any).width
    });
  }
}