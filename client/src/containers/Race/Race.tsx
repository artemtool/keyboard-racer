import './Race.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../../state/appState';
import RacePath from './RacePath';
import Typer from './Typer';
import { History } from 'history';
import { match } from 'react-router-dom';
import { joinRace, updateRaceProgress } from './../../state/actions/race';
import * as routes from '../../constants/routes';
import { CarModel } from '../../../../shared/enums/carModel';
import CarPicker from '../../components/CarPicker/CarPicker';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { RaceStatus } from '../../../../shared/enums/raceStatus';
import { apiManager } from './../../index';
import { AuthorizationType } from '../../common/apiManager';
import { IArticle } from './../../../../shared/interfaces/article';
import { Car } from './../../models/car';
import { RaceMember } from './../../models/raceMember';

interface RouteParams {
  id: string;
}

interface Props {
  userId: string;
  userCars: Array<Car>;
  history: History;
  match: match<RouteParams>;
  raceId: string;
  raceMembers: Array<RaceMember>;
  raceCountdown: number;
  raceStatus: RaceStatus;
  raceArticle: IArticle;
  joinRace: (id: string, car: CarModel) => Promise<void>;
  updateRaceProgress: (id: string, progress: number) => Promise<void>;
}

interface State {
  isCarPickerShown: boolean;
}

class RacePage extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      isCarPickerShown: false
    };

    this.onTypedLengthChange = this.onTypedLengthChange.bind(this);
    this.onCarPick = this.onCarPick.bind(this);
  }

  componentDidMount() {
    const raceId: string = this.props.match.params.id;

    this.isCarPicked(raceId)
      .then(isCarPicked => {
        if (isCarPicked || this.props.userCars.length === 1) {
          this.props.joinRace(raceId, this.props.userCars[0].model);
        } else {
          this.toggleCarPicker();
        }
      })
      .catch((error: Error) => {
        alert(`Error! ${error.message}`);
        this.props.history.push(routes.DASHBOARD);
      });
  }

  public render() {
    const invitationUrl = `${document.location.origin}${routes.RACE}/${this.props.raceId}`;
    const currentUser = this.props.raceMembers.find(m => m.userId === this.props.userId);
    return (
      this.state.isCarPickerShown ? (
        <section className="car-picker">
          <h2>Choose racing car</h2>
          <CarPicker
            cars={this.props.userCars}
            onPick={this.onCarPick}
          />
        </section>
      ) : (
          <section className="race">
            <h2>
              Race #{this.props.raceId}
              <CopyToClipboard text={invitationUrl}>
                <button type="button">Copy invitation URL</button>
              </CopyToClipboard>

            </h2>
            <h2>Status: {this.props.raceStatus}</h2>
            {
              this.props.raceMembers.map(member =>
                <RacePath
                  key={member.userId}
                  progress={this.getRaceMemberProgress(this.props.raceArticle.text.length, member.typedLength)}
                  nickname={member.userNickname}
                  CPM={member.CPM}
                  car={member.car}
                  place={member.place}
                />
              )
            }
            {
              currentUser && (
                <Typer
                  disabled={this.props.raceStatus !== RaceStatus.Started || !!currentUser.place}
                  text={this.props.raceArticle.text}
                  initialTypedLength={currentUser.typedLength}
                  onTypedLengthChange={this.onTypedLengthChange}
                />
              )
            }
            {
              this.props.raceStatus === RaceStatus.MembersWaiting && this.props.raceCountdown > 0 && (
                <h2>Countdown: {this.props.raceCountdown}</h2>
              )
            }
          </section>
        )
    );
  }

  private onTypedLengthChange(typedLength: number) {
    this.props.updateRaceProgress(this.props.raceId, typedLength);
  }

  private isCarPicked(raceId: string): Promise<boolean> {
    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory
    }, `http://localhost:3003/race/isCarPicked/${raceId}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.status === 404) {
          throw new Error('Race was not found.');
        } else {
          return response.json();
        }
      });
  }

  private toggleCarPicker() {
    this.setState(state => ({
      isCarPickerShown: !state.isCarPickerShown
    }));
  }

  private onCarPick(car: Car) {
    this.props.joinRace(this.props.match.params.id, car.model)
      .then(() => this.setState({
        isCarPickerShown: false
      }))
      .catch((error: Error) => {
        alert(`Error! ${error.message}`);
        this.props.history.push(routes.DASHBOARD);
      });
  }

  private getRaceMemberProgress(articleLength: number, typedLength: number) {
    return 100 - Math.ceil((articleLength - typedLength) * 100 / articleLength);
  }
}

const mapStateToProps = (state: AppState) => ({
  userId: state.user.authUser.id,
  userCars: state.user.authUser.cars,
  raceId: state.race.raceId,
  raceMembers: state.race.members,
  raceCountdown: state.race.countdown,
  raceStatus: state.race.status,
  raceArticle: state.race.article
});

const mapDispatchToProps = dispatch => ({
  joinRace: (id: string, car: CarModel) => dispatch(joinRace(id, car)),
  updateRaceProgress: (id: string, progress: number) => dispatch(updateRaceProgress(id, progress))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RacePage);