import * as React from 'react';
import { AppState } from './../../state/appState';
import { connect } from 'react-redux';
import { match } from 'react-router-dom';
import SignInForm from './SignInForm';
import SignUpForm from './SignUpForm';
import { signInWithEmail, signUpWithEmail } from './../../state/actions/user';
import { SignUpRequest } from './../../../../shared/interfaces/dto/signUpRequest';
import './Auth.scss';
import { History } from 'history';
import * as routes from '../../constants/routes';

type RouteParams = {
  tab: string;
};

interface Props {
  history: History;
  match: match<RouteParams>;
  isLoading: boolean;
  signInWithEmail: (email: string, password: string) => Promise<void>;
  signUpWithEmail: (request: SignUpRequest) => Promise<void>;
}

interface State {
  selectedTab: string;
}

class AuthPage extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);

    const initialTab = this.getTabFromParam(this.props.match.params.tab);
    this.state = {
      selectedTab: initialTab
    };

    this.signIn = this.signIn.bind(this);
    this.signUp = this.signUp.bind(this);
  }

  public render() {
    return (
      <div className="tabs-container">
        <div className={`tabs-container__tab${this.state.selectedTab != 'signin' ? '--hidden' : ''}`}>
          <SignInForm
            isLoading={this.props.isLoading}
            onSubmit={this.signIn}
          />
        </div>
        <div className={`tabs-container__tab${this.state.selectedTab != 'signup' ? '--hidden' : ''}`}>
          <SignUpForm
            isLoading={this.props.isLoading}
            onSubmit={this.signUp}
          />
        </div>
      </div>
    );
  }

  public componentDidUpdate(prevProps: Props) {
    if (prevProps.match.params.tab != this.props.match.params.tab) {
      const tab = this.getTabFromParam(this.props.match.params.tab);
      this.setState({
        selectedTab: tab
      });
    }
  }

  private signIn(email: string, password) {
    this.props.signInWithEmail(email, password);
  }

  private signUp(request: SignUpRequest) {
    this.props.signUpWithEmail(request)
      .then(() => this.props.history.push(routes.SIGN_IN));
  }

  private getTabFromParam(tabParam: string): string {
    if (tabParam == 'signup') {
      return tabParam;
    } else {
      return 'signin';
    }
  }
}

const mapStateToProps = (state: AppState) => ({
  isLoading: state.user.isLoading
});

const mapDispatchToProps = dispatch => ({
  signInWithEmail: (email: string, password: string) => dispatch(signInWithEmail(email, password)),
  signUpWithEmail: (request: SignUpRequest) => dispatch(signUpWithEmail(request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthPage);