import * as React from 'react';
import { SignUpRequest } from '../../../../shared/interfaces/dto/signUpRequest';
import './Auth.scss';

interface Props {
  isLoading: boolean;
  onSubmit: (data: SignUpRequest) => void;
}

interface State {
  email: string;
  password: string;
  repeatedPassword: string;
  nickname: string;
}

export default class SignUpForm extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      repeatedPassword: '',
      nickname: ''
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  public render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="form-field">
          <label htmlFor="nickname">Nickname</label>
          <input
            id="nickname"
            type="text"
            name="nickname"
            placeholder="Enter your nickname"
            value={this.state.nickname}
            onChange={this.onValueChange}
          />
        </div>
        <div className="form-field">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            placeholder="Enter your email"
            value={this.state.email}
            onChange={this.onValueChange}
          />
        </div>
        <div className="form-field">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            name="password"
            placeholder="Enter your password"
            value={this.state.password}
            onChange={this.onValueChange}
          />
        </div>
        <div className="form-field">
          <label htmlFor="password">Repeat password</label>
          <input
            id="repeatedPassword"
            type="password"
            name="repeatedPassword"
            placeholder="Enter the password above"
            value={this.state.repeatedPassword}
            onChange={this.onValueChange}
          />
        </div>
        <button type="submit">Sign up</button>
      </form>
    );
  }

  private onValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  }

  private onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    // validation, resseting, etc.

    this.props.onSubmit(this.state);
  }
}