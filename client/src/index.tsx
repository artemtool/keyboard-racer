import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { compose } from 'recompose';
import thunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import App from './App';
import appReducer from './state/reducers/index';
import rootSaga from './state/sagas';
import { APIManager } from './common/apiManager';
import { ACCESS_TOKEN, REFRESH_TOKEN } from './constants/keys';
import { showLoader, hideLoader } from './state/actions/layout';
import { SocketManager } from './common/socketManager';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [
  applyMiddleware(
    thunkMiddleware,
    sagaMiddleware
  )
];
if (window['__REDUX_DEVTOOLS_EXTENSION__']) {
  middlewares.push(window['__REDUX_DEVTOOLS_EXTENSION__']())
}

export const store = createStore(
  appReducer,
  compose(...middlewares)
);
sagaMiddleware.run(rootSaga, store.dispatch);

export const apiManager = new APIManager({
  showLoader: () => store.dispatch(showLoader()),
  hideLoader: () => store.dispatch(hideLoader()),
  authorizationTokenFactory: () => localStorage.getItem(ACCESS_TOKEN),
  refreshTokenFactory: () => localStorage.getItem(REFRESH_TOKEN)
});

export const socketManager = new SocketManager({
  url: 'http://localhost:3003',
  reconnectionAttempts: 1,
  authorizationTokenFactory: () => localStorage.getItem(ACCESS_TOKEN)
});

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('app-root')
);
// registerServiceWorker();
