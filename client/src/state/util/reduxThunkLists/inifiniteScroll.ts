import { Dispatch, Action } from 'redux';
import { InfiniteScrollListState, ListParameters, RequestAction, ReceiveAction, FailureAction, ChangeIsDescAction, ChangeOrderByAction } from './common/interfaces';
import { createRequestAction, RECEIVE_ACTION_PREFIX, createReceiveAction, REQUEST_ACTION_PREFIX, FAILURE_ACTION_PREFIX, createErrorAction, CHANGE_ORDER_BY_PREFIX, CHANGE_IS_DESC_PREFIX } from './common/actions';

const isDataSelectAllowed = <T>(featureState: InfiniteScrollListState<T>): boolean => {
  return !featureState.isLoading && featureState.canLoadMore;
}

export const createThunk = <S, T>(
  selectFeatureState: (state: S) => InfiniteScrollListState<T>,
  selectData: (parameters: ListParameters, state: S) => Promise<any>,
  isInitialLoad: boolean,
  actionsSuffix: string,
  mapData?: (result: any) => { entities: Array<T>, total: number }
): Function => {
  return (dispatch: Dispatch, getState: () => S) => {
    const state = getState();
    const featureState = selectFeatureState(state);

    if (isDataSelectAllowed(featureState) || isInitialLoad) {
      dispatch(createRequestAction(actionsSuffix, isInitialLoad));

      const parameters: ListParameters = {
        page: isInitialLoad
          ? 1
          : featureState.page,
        perPageAmount: featureState.perPageAmount,
        isDesc: featureState.isDesc,
        orderBy: featureState.orderBy
      };

      return selectData(parameters, state)
        .then(result => result.json())
        .then(result => {
          const { entities, total } = mapData
            ? mapData(result)
            : result;
          dispatch(createReceiveAction<T>(actionsSuffix, entities, parameters.page, total));
        })
        .catch(error => {
          dispatch(createErrorAction(actionsSuffix, error));
          throw error;
        });
    } else {
      return Promise.resolve();
    }
  }
};


const createActionHandlers = <T>(actionsSuffix: string) => ({
  [REQUEST_ACTION_PREFIX + actionsSuffix]: (state: InfiniteScrollListState<T>, action: RequestAction): InfiniteScrollListState<T> => {
    const entities = action.isInitialLoad
      ? []
      : state.entities;
    const canLoadMore = action.isInitialLoad
      ? true
      : state.canLoadMore;
    const page = action.isInitialLoad
      ? 1
      : state.page;
    const total = action.isInitialLoad
      ? 0
      : state.total;

    return {
      ...state,
      entities,
      total,
      canLoadMore,
      page,
      isLoading: true
    }
  },
  [RECEIVE_ACTION_PREFIX + actionsSuffix]: (state: InfiniteScrollListState<T>, action: ReceiveAction<T>): InfiniteScrollListState<T> => {
    const canLoadMore = action.entities.length > 0;
    let total = 0;
    if (action.total > 0) {
      total = action.total;
    }

    return {
      ...state,
      entities: [...state.entities, ...action.entities],
      total: state.page === 1
        ? total
        : state.total,
      page: state.page + 1,
      isLoading: false,
      canLoadMore
    }
  },
  [FAILURE_ACTION_PREFIX + actionsSuffix]: (state: InfiniteScrollListState<T>, action: FailureAction): InfiniteScrollListState<T> => {
    return {
      ...state,
      isLoading: false
    }
  },
  [CHANGE_ORDER_BY_PREFIX + actionsSuffix]: (state: InfiniteScrollListState<T>, action: ChangeOrderByAction): InfiniteScrollListState<T> => {
    return {
      ...state,
      orderBy: action.orderBy
    }
  },
  [CHANGE_IS_DESC_PREFIX + actionsSuffix]: (state: InfiniteScrollListState<T>, action: ChangeIsDescAction): InfiniteScrollListState<T> => {
    return {
      ...state,
      isDesc: action.isDesc
    }
  }
});

export const addActionHandlers = <T>(wrapperReducer, actionsSuffix: string) => {
  const listActionHandlers: { [key: string]: (state: InfiniteScrollListState<T>, action: Action) => InfiniteScrollListState<T> } = createActionHandlers<T>(actionsSuffix);
  return (state: InfiniteScrollListState<T>, action: Action): InfiniteScrollListState<T> => {
    if (listActionHandlers[action.type]) {
      return listActionHandlers[action.type](state, action);
    } else {
      return wrapperReducer(state, action);
    }
  }
};