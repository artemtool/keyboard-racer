import { Dispatch, Action } from 'redux';
import { PaginationListState, ListParameters, RequestAction, ReceiveAction, FailureAction, ChangeIsDescAction, ChangeOrderByAction } from './common/interfaces';
import { createRequestAction, RECEIVE_ACTION_PREFIX, createReceiveAction, REQUEST_ACTION_PREFIX, FAILURE_ACTION_PREFIX, createErrorAction, CHANGE_ORDER_BY_PREFIX, CHANGE_IS_DESC_PREFIX } from './common/actions';

export const createThunk = <S, T>(
  selectFeatureState: (state: S) => PaginationListState<T>,
  selectData: (parameters: ListParameters, state: S) => Promise<any>,
  isInitialLoad: boolean,
  page: number,
  actionsSuffix: string,
  mapData?: (result: any) => { entities: Array<T>, total: number }
): Function => {
  return (dispatch: Dispatch, getState: () => S) => {
    const state = getState();
    const featureState = selectFeatureState(state);

    dispatch(createRequestAction(actionsSuffix, isInitialLoad));

    const parameters: ListParameters = {
      page,
      perPageAmount: featureState.perPageAmount,
      isDesc: featureState.isDesc,
      orderBy: featureState.orderBy
    };

    return selectData(parameters, state)
      .then(result => result.json())
      .then(result => {
        const { entities, total } = mapData
          ? mapData(result)
          : result;
        dispatch(createReceiveAction<T>(actionsSuffix, entities, parameters.page, total));
      })
      .catch(error => {
        dispatch(createErrorAction(actionsSuffix, error));
        throw error;
      });
  }
};


const createActionHandlers = <T>(actionsSuffix: string) => ({
  [REQUEST_ACTION_PREFIX + actionsSuffix]: (state: PaginationListState<T>, action: RequestAction): PaginationListState<T> => {
    const entities = action.isInitialLoad
      ? []
      : state.entities;
    const total = action.isInitialLoad
      ? null
      : state.total;
    const currentPage = action.isInitialLoad
      ? null
      : state.currentPage;

    return {
      ...state,
      entities,
      total,
      currentPage,
      isLoading: true
    }
  },
  [RECEIVE_ACTION_PREFIX + actionsSuffix]: (state: PaginationListState<T>, action: ReceiveAction<T>): PaginationListState<T> => {
    const entities = [...state.entities];
    entities[action.page] = action.entities;

    return {
      ...state,
      entities,
      total: action.total || null,
      currentPage: action.page,
      isLoading: false
    }
  },
  [FAILURE_ACTION_PREFIX + actionsSuffix]: (state: PaginationListState<T>, action: FailureAction): PaginationListState<T> => {
    return {
      ...state,
      isLoading: false
    }
  },
  [CHANGE_ORDER_BY_PREFIX + actionsSuffix]: (state: PaginationListState<T>, action: ChangeOrderByAction): PaginationListState<T> => {
    return {
      ...state,
      orderBy: action.orderBy
    }
  },
  [CHANGE_IS_DESC_PREFIX + actionsSuffix]: (state: PaginationListState<T>, action: ChangeIsDescAction): PaginationListState<T> => {
    return {
      ...state,
      isDesc: action.isDesc
    }
  }
});

export const addActionHandlers = <T>(wrapperReducer, actionsSuffix: string) => {
  const listActionHandlers: { [key: string]: (state: PaginationListState<T>, action: Action) => PaginationListState<T> } = createActionHandlers<T>(actionsSuffix);
  return (state: PaginationListState<T>, action: Action): PaginationListState<T> => {
    if (listActionHandlers[action.type]) {
      return listActionHandlers[action.type](state, action);
    } else {
      return wrapperReducer(state, action);
    }
  }
};