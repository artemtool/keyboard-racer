import { RequestAction, ReceiveAction, FailureAction, ChangeOrderByAction, ChangeIsDescAction } from './interfaces';

export const REQUEST_ACTION_PREFIX = 'REQUEST_';
export const RECEIVE_ACTION_PREFIX = 'RECEIVE_';
export const FAILURE_ACTION_PREFIX = 'FAILURE_';
export const CHANGE_ORDER_BY_PREFIX = 'CHANGE_ORDER_BY_';
export const CHANGE_IS_DESC_PREFIX = 'CHANGE_IS_DESC_';

export const createRequestAction = (actionSuffix: string, isInitialLoad: boolean): RequestAction => ({
  type: REQUEST_ACTION_PREFIX + actionSuffix,
  isInitialLoad
});

export const createReceiveAction = <T>(actionSuffix: string, entities: Array<T>, page: number, total: number): ReceiveAction<T> => ({
  type: RECEIVE_ACTION_PREFIX + actionSuffix,
  entities,
  page,
  total
});

export const createErrorAction = (actionSuffix: string, error: Error): FailureAction => ({
  type: FAILURE_ACTION_PREFIX + actionSuffix,
  error
});

export const createChangeOrderByAction = (actionSuffix: string, orderBy: string): ChangeOrderByAction => ({
  type: CHANGE_ORDER_BY_PREFIX + actionSuffix,
  orderBy
});

export const createChangeIsDescAction = (actionSuffix: string, isDesc: boolean): ChangeIsDescAction => ({
  type: CHANGE_IS_DESC_PREFIX + actionSuffix,
  isDesc
});