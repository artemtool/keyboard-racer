import { apiManager } from '../..';
import { AuthorizationType } from '../../common/apiManager';
import { AppState } from './../appState';
import { createChangeIsDescAction, createChangeOrderByAction } from '../util/reduxThunkLists/common/actions';
import { IRaceHistoryItem } from '../../../../shared/interfaces/raceHistoryItem';
import { ListParameters, ChangeOrderByAction, ChangeIsDescAction } from '../util/reduxThunkLists/common/interfaces';
import { createThunk } from './../util/reduxThunkLists/pagination';

export const ACTIONS_SUFFIX = 'RACE_HISTORY';

export function fetchRaceHistoryPage(page: number, isInitialLoad: boolean = false) {
  const headers = {
    'Content-type': 'application/json'
  };

  const featureStateSelector = (state: AppState) => state.raceHistory;
  const dataSelector = (parameters: ListParameters, state: AppState) => {
    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory,
      isGlobalLoaderShown: false
    }, 'http://localhost:3003/race/history', {
        method: 'POST',
        headers,
        body: JSON.stringify(parameters)
      });
  };

  return createThunk<AppState, IRaceHistoryItem>(
    featureStateSelector,
    dataSelector,
    isInitialLoad,
    page,
    ACTIONS_SUFFIX
  );
}

export function changeOrderBy(orderBy: string): ChangeOrderByAction {
  return createChangeOrderByAction(ACTIONS_SUFFIX, orderBy);
}

export function changeIsDesc(isDesc: boolean): ChangeIsDescAction {
  return createChangeIsDescAction(ACTIONS_SUFFIX, isDesc);
}