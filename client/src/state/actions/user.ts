import { Dispatch, Action } from "redux";
import { FailureAction } from "../appState";
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../constants/keys';
import { User } from "../../models/user";
import { SignUpRequest } from '../../../../shared/interfaces/dto/signUpRequest';
import { apiManager } from './../../index';
import { AuthorizationType } from "../../common/apiManager";
import { UpdateUserSuccessAction } from './user';

export const SIGN_IN = 'SIGN_IN';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';
export const SIGN_UP = 'SIGN_UP';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
export const SIGN_OUT = 'SIGN_OUT';
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';
export const SIGN_OUT_FAILURE = 'SIGN_OUT_FAILURE';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';

export interface SignInSuccessAction extends Action {
  user: User;
}

export interface UpdateUserSuccessAction extends Action {
  user: User;
}

export function startSignIn(): Action {
  return {
    type: SIGN_IN
  }
}

export function signInSuccess(user: User): SignInSuccessAction {
  return {
    type: SIGN_IN_SUCCESS,
    user
  }
}

export function signInFailure(error): FailureAction {
  return {
    type: SIGN_IN_FAILURE,
    error
  }
}

export function signInWithEmail(email: string, password: string) {
  return (dispatch: Dispatch) => {
    dispatch(startSignIn());
    return apiManager.fetch(null, `http://localhost:3003/user/signin`, {
      body: JSON.stringify({
        email: email,
        password: password
      }),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(result => {
        localStorage.setItem(ACCESS_TOKEN, result.accessToken);
        localStorage.setItem(REFRESH_TOKEN, result.refreshToken);

        const user = new User(result.user);
        dispatch(signInSuccess(user));
        return user;
      })
      .catch(error => {
        dispatch(signInFailure(error));
        throw error;
      })
  }
}

export function trySignIn() {
  return (dispatch: Dispatch) => {

    if (localStorage.getItem(ACCESS_TOKEN)) {
      dispatch(startSignIn());

      const headers = {
        'Content-Type': 'application/json'
      };

      return apiManager.fetch({
        authorizationType: AuthorizationType.Mandatory
      }, `http://localhost:3003/user/iam`, {
          method: 'GET',
          headers
        })
        .then(response => response.json())
        .then(user => {
          dispatch(signInSuccess(new User(user)));
        })
        .catch(error => {
          dispatch(signInFailure(error));
        })
    } else {
      return Promise.resolve();
    }
  }
}

export function startSignUp(): Action {
  return {
    type: SIGN_UP
  }
}

export function signUpSuccess(): Action {
  return {
    type: SIGN_UP_SUCCESS
  }
}

export function signUpFailure(error): FailureAction {
  return {
    type: SIGN_UP_FAILURE,
    error
  }
}

export function signUpWithEmail(request: SignUpRequest) {
  return (dispatch: Dispatch) => {
    dispatch(startSignUp());
    return apiManager.fetch(null, `http://localhost:3003/user/signup`, {
      body: JSON.stringify(request),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => dispatch(signUpSuccess()))
      .catch(error => {
        dispatch(signUpFailure(error));
        throw error;
      })
  }
}

export function startSignOut(): Action {
  return {
    type: SIGN_OUT
  }
}

export function signOutSuccess(): Action {
  return {
    type: SIGN_OUT_SUCCESS
  }
}

export function signOutFailure(error): FailureAction {
  return {
    type: SIGN_OUT_FAILURE,
    error
  }
}

export function signOut() {
  return (dispatch: Dispatch) => {
    dispatch(startSignOut());

    const headers = {
      'Content-Type': 'application/json'
    };

    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory
    }, `http://localhost:3003/user/signout`, {
        method: 'POST',
        headers
      })
      .then(() => {
        localStorage.removeItem(ACCESS_TOKEN);
        localStorage.removeItem(REFRESH_TOKEN);

        dispatch(signOutSuccess());
      })
      .catch(error => {
        dispatch(signOutFailure(error));
        throw error;
      })
  }
}

export function updateUserSuccess(user: User): UpdateUserSuccessAction {
  return {
    type: UPDATE_USER_SUCCESS,
    user
  }
}