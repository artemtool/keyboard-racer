import { combineReducers } from 'redux';
import user from './user';
import race from './race';
import layout from './layout';
import raceHistory from './raceHistory';
import userRating from './userRating';
import carsOnSale from './carsOnSale';

const appReducer = combineReducers({
  user,
  race,
  layout,
  raceHistory,
  userRating,
  carsOnSale
});

export default appReducer;