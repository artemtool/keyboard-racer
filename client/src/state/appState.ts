import { State as UserState } from "./reducers/user";
import { State as RaceState } from "./reducers/race";
import { State as LayoutState } from "./reducers/layout";
import { Action } from 'redux';
import { IRaceHistoryItem } from "../../../shared/interfaces/raceHistoryItem";
import { PaginationListState } from './util/reduxThunkLists/common/interfaces';
import { User } from "../models/user";
import { Car } from './../models/car';

export interface AppState {
  user: UserState;
  race: RaceState;
  layout: LayoutState;
  raceHistory: PaginationListState<IRaceHistoryItem>;
  userRating: PaginationListState<User>;
  carsOnSale: PaginationListState<Car>;
};

export interface FailureAction extends Action {
  error: any;
} 