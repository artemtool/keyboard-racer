import * as io from 'socket.io-client';

interface SocketManagerConfig {
  url: string;
  reconnectionAttempts: number;
  authorizationTokenFactory: () => string | null;
}

export class SocketManager {
  private config: SocketManagerConfig;
  private ioSocket: SocketIOClient.Socket;

  constructor(config: SocketManagerConfig) {
    this.config = config;
  }

  public connect(payload: Object = {}) {
    this.ioSocket = io.connect(this.config.url, {
      reconnectionAttempts: this.config.reconnectionAttempts,
      query: {
        accessToken: this.config.authorizationTokenFactory(),
        ...payload
      }
    });
  }

  public get socket(): SocketIOClient.Socket {
    return this.ioSocket;
  }
}