import { Car } from './car';
import { IRaceMember } from '../../../shared/interfaces/raceMember';

export class RaceMember {
  userId: string;
  userNickname: string;
  car: Car;
  typedLength: number;
  CPM: number;
  place: number;

  constructor(member: IRaceMember) {
    this.userId = member.userId;
    this.userNickname = member.userNickname;
    this.car = new Car({ model: member.car })
    this.typedLength = member.typedLength;
    this.CPM = member.CPM;
    this.place = member.place;
  }
}