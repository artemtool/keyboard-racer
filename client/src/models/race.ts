import { IRace } from './../../../shared/interfaces/race';
import { RaceStatus } from '../../../shared/enums/raceStatus';
import { IArticle } from './../../../shared/interfaces/article';
import { RaceMember } from './raceMember';

export class Race {
  id: string;
  creatorId: string;
  rivalId: string;
  winnerId: string;
  date: Date;
  members: Array<RaceMember>;
  membersNeeded: number;
  status: RaceStatus;
  article: IArticle;

  constructor(race: IRace) {
    this.id = race._id;
    this.creatorId = race.creatorId;
    this.winnerId = race.winnerId;
    this.date = new Date(race.date);
    this.members = race.members
      .map(member => new RaceMember(member));
    this.membersNeeded = race.membersNeeded;
    this.status = race.status;
    this.article = race.article;
  }
}