import { CarModel } from "../../../shared/enums/carModel";
import { ICar } from './../../../shared/interfaces/car';

export class Car {
  model: CarModel;
  price: number;
  imageUrl: string;
  label: string;

  constructor(car: ICar) {
    this.model = car.model;
    this.price = car.price;
    this.imageUrl = getCarImageUrl(car.model);
    this.label = getCarLabel(car.model);
  }
}

const pathToImages = '/public/assets/images';
const fileType = '.png';

const garage = {
  [CarModel.Audi_TT]: `Audi_TT`,
  [CarModel.Chevrolet_Corvete]: 'Chevrolet_Corvete',
  [CarModel.Chevrolet_Malibu]: 'Chevrolet_Malibu',
  [CarModel.Lamborghini_Huracan]: 'Lamborghini_Huracan',
  [CarModel.Nissan_GTR]: 'Nissan_GTR',
};

function getCarImageUrl(car: CarModel) {
  const imageName = garage[car];
  if (imageName) {
    return `${pathToImages}/${imageName + fileType}`;
  } else {
    return null;
  }
};

function getCarLabel(car: CarModel) {
  const imageName = garage[car];
  if (imageName) {
    return imageName.split('_').join(' ');
  } else {
    return null;
  }
};