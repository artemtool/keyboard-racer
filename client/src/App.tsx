import * as React from 'react';
import {
  Switch,
  withRouter
} from 'react-router-dom';
import AuthPage from './containers/Auth/Auth';
import RacePage from './containers/Race/Race';
import ProtectedRoute from './components/ProtectedRoute';
import Header from './components/Header/Header';
import * as routes from './constants/routes';
import { User } from './enums/user';
import './App.scss';
import DashboardPage from './containers/Dashboard/Dashboard';
import GaragePage from './containers/Garage/Garage';
import { connect } from 'react-redux';
import { AppState } from './state/appState';
import { compose } from 'recompose';
import Loader from './components/Loader/Loader';

interface Props {
  isLoaderShown: boolean;
}

class App extends React.Component<Props> {

  public render() {
    return (
      <React.Fragment>
        <Header />
        <main>
          <Switch>
            <ProtectedRoute
              path={`${routes.AUTH}/:tab`}
              component={AuthPage}
              onlyFor={User.Anonymous}
              redirectPath={routes.HOME}
            />
            <ProtectedRoute
              path={routes.DASHBOARD}
              component={DashboardPage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              path={`${routes.RACE}/:id`}
              component={RacePage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
            <ProtectedRoute
              path={routes.GARAGE}
              component={GaragePage}
              onlyFor={User.Authorized}
              redirectPath={routes.SIGN_IN}
            />
          </Switch>
        </main>
        <footer>

        </footer>
        <Loader shown={this.props.isLoaderShown} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  isLoaderShown: state.layout.totalLoaders > 0
});

export default compose(
  withRouter,
  connect(
    mapStateToProps
  )
)(App);