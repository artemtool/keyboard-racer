export enum User {
  Anonymous = 0,
  Authorized = 1,
  Administrator = 2
}