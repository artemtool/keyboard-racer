import * as React from 'react';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { trySignIn, signOut } from '../../state/actions/user';
import withAuthentication from '../../components/withAuthentication';
import { AppState } from './../../state/appState';
import { User } from '../../models/user';
import './Header.scss';

interface Props {
  user: User,
  signOut: () => Promise<void>,
  trySignIn: () => Promise<void>
}

class Header extends React.PureComponent<Props> {
  render() {
    return (
      <header>
        <nav>
          <ul>
            {
              !this.props.user && (
                <React.Fragment>
                  <li><Link to={routes.SIGN_IN}>Sign In</Link></li>
                  <li><Link to={routes.SIGN_UP}>Sign Up</Link></li>
                </React.Fragment>
              )
            }
            {
              this.props.user && (
                <React.Fragment>
                  <li><Link to={routes.DASHBOARD}>Dashboard</Link></li>
                  <li><Link to={routes.GARAGE}>Garage</Link></li>
                  <li className="sign-out"><a onClick={this.props.signOut}>Sign Out</a></li>
                </React.Fragment>

              )
            }
          </ul>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  user: state.user.authUser
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
  trySignIn: () => dispatch(trySignIn())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthentication
)(Header);