import * as React from 'react';
import './Loader.scss';

interface Props {
  shown: boolean;
}

export default class Loader extends React.PureComponent<Props> {
  render() {
    return this.props.shown && (
      <div className="loader-overlay">
        <img src="/public/assets/images/loader.gif" alt="Loader"></img>
      </div>
    );
  }
}