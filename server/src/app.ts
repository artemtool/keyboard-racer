import * as express from 'express';
import * as http from 'http';
import routes from './routes';
import { configureManagers } from './managers';

const app = express();
app.use('/', routes);

const httpServer = new http.Server(app);
httpServer.listen(3003, () => console.debug(`NodeJS API started on port 3003!`));

(async () => {
  try {
    await configureManagers(httpServer);
  } catch (error) {
    console.log('Error during managers init');
  }
})();