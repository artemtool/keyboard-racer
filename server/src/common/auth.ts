import * as jwt from 'jsonwebtoken';

export function verifyJWTToken(token: string) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
      if (err || !decodedToken) {
        return reject(err);
      }

      resolve(decodedToken);
    });
  });
}

export function createJWToken(options: jwt.SignOptions = {}, payload: any) {
  if (!options.expiresIn) {
    options.expiresIn = 3600;
  }

  let token = jwt.sign({
    payload
  }, process.env.JWT_SECRET, {
      expiresIn: options.expiresIn,
      algorithm: 'HS256'
    });

  return token;
}