import * as express from 'express';
import * as bodyParser from 'body-parser';
import user from './user';
import race from './race';
import car from './car';
import { handleCors, handleErrors } from './../middlewares';

const router = express.Router();

router.use(handleCors, bodyParser.json(), handleErrors);

router.use('/user', user);
router.use('/race', race);
router.use('/car', car);

export default router;