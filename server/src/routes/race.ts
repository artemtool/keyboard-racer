import * as express from 'express';
import { verifyJWT } from '../middlewares';
import { IRace } from './../../../shared/interfaces/race';
import { CarModel } from '../../../shared/enums/carModel';
import { raceManager } from '../managers';
import { IRaceMember } from '../../../shared/interfaces/raceMember';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IPage } from '../../../shared/interfaces/page';

const router = express.Router();

router.post('/create', verifyJWT, async (req, res) => {
  const userId = req['decodedToken'].payload.userId;
  const membersNeeded = req.body.membersNeeded;
  const race = await raceManager.createRace(userId, membersNeeded);

  res.send(race);
});

router.post('/join/:id', verifyJWT, async (req, res) => {
  const raceId: string = req.params.id;
  const userId: string = req['decodedToken'].payload.userId;
  const car: CarModel = req.body.car;
  const race = await raceManager.joinRace(raceId, userId, car);

  if (race) {
    res.send(race);
  } else {
    res.sendStatus(403);
  }
});

router.get('/isCarPicked/:id', verifyJWT, async (req, res) => {
  const raceId: string = req.params.id;
  const userId: string = req['decodedToken'].payload.userId;
  const race: IRace = await raceManager.getRaceById(raceId);

  if (race) {
    const raceMember: IRaceMember = race.members.find(m => m.userId === userId);
    res.send(!!raceMember && raceMember.car != null);
  } else {
    res.sendStatus(404);
  }
});

router.post('/history', verifyJWT, async (req, res) => {
  const userId: string = req['decodedToken'].payload.userId;
  const parameters: PageableListParameters = req.body;
  const page: IPage<IRace> = await raceManager.getUserRaces(userId, parameters);

  res.send(page);
});

export default router;