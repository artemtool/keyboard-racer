import * as socketIO from 'socket.io';
import * as http from 'http';
import { NextFunction } from 'connect';

export class SocketsManager {
  private httpServer: SocketIO.Server;
  private verifyJWT: (socket: socketIO.Socket, next: NextFunction) => void;
  private eventHandlers: Array<(socket: socketIO.Socket) => void> = [];
  private onConnectHandlers: Array<(socket: socketIO.Socket) => void> = [];

  constructor(
    server: http.Server,
    verifyJWT: (socket: socketIO.Socket, next: NextFunction) => void,
    onConnectHandlers: Array<(socket: socketIO.Socket) => void> = [],
    eventHandlers: Array<(socket: socketIO.Socket) => void> = []
  ) {
    this.httpServer = socketIO(server);
    this.verifyJWT = verifyJWT;
    this.onConnectHandlers.push(...onConnectHandlers);
    this.eventHandlers.push(...eventHandlers);
  }

  public listen() {
    this.httpServer
      .use(this.verifyJWT)
      .on('connection', async (socket: socketIO.Socket) => {
        for (let onConnectHandler of this.onConnectHandlers) {
          await onConnectHandler(socket);
        }

        for (let handler of this.eventHandlers) {
          await handler(socket);
        }
      });
  }

  public addEventHandlers(handlers: Array<(socket: socketIO.Socket) => void>) {
    this.eventHandlers.push(...handlers);
  }

  public addOnConnectHandlers(handlers: Array<(socket: socketIO.Socket) => void>) {
    this.onConnectHandlers.push(...handlers);
  }
}