import * as http from 'http';
import * as socketIO from 'socket.io';
import { SocketsManager } from './socketsManager';
import { DBConnectionManager } from './dbConnectionManager';
import { verifyJWTOnSocketConnection } from '../middlewares';
import { IRace } from './../../../shared/interfaces/race';
import { IRaceMember } from './../../../shared/interfaces/raceMember';
import { RaceStatus } from '../../../shared/enums/raceStatus';
import { UserManager } from './userManager';
import { RaceManager } from './raceManager';
import { CollectionManager } from './collectionManager';
import { CarManager } from './carManager';

const socketOnConnectHandlers: Array<(socket: socketIO.Socket) => void> = [
  async (socket: socketIO.Socket) => {
    const userId = socket['decodedToken'].payload.userId;
    const raceId = socket.handshake.query && socket.handshake.query.raceId;
    socket.join(raceId, async () => {
      const race: IRace = await raceManager.getRaceById(raceId);
      const raceMember: IRaceMember = race.members.find(m => m.userId === userId);
      socket.broadcast.to(raceId).emit('raceProgressUpdate', raceId, raceMember);

      if (race.members.length === race.membersNeeded && race.status === RaceStatus.MembersWaiting) {
        let countdown = 3;

        const timer = setInterval(async () => {
          if (countdown < 0) {
            clearInterval(timer);

            race.status = RaceStatus.Started;
            raceManager.updateRace(raceId, race);
            socket.broadcast.to(raceId).emit('raceStatusUpdate', raceId, race.status);
            socket.emit('raceStatusUpdate', raceId, race.status);
          } else {
            socket.broadcast.to(raceId).emit('countdownUpdate', raceId, countdown);
            socket.emit('countdownUpdate', raceId, countdown);

            countdown -= 1;
          }
        }, 1000);
      }
    });
  }
];

const socketEventHandlers: Array<(socket: socketIO.Socket) => void> = [
  (socket: socketIO.Socket) => {
    const userId = socket['decodedToken'].payload.userId;
    socket.on('updateRaceProgress', async (raceId: string, typedLength: number) => {
      const race: IRace = await raceManager.getRaceById(raceId);
      const raceMember: IRaceMember = race.members.find(m => m.userId === userId);

      if (raceMember.typedLength < race.article.text.length) {
        if (typedLength === race.article.text.length) {
          const finishedAmount = race.members.filter(m => m.typedLength === race.article.text.length).length;
          raceMember.place = finishedAmount + 1;

          if (finishedAmount + 1 === race.membersNeeded) {
            race.status = RaceStatus.Finished;

            socket.broadcast.to(raceId).emit('raceStatusUpdate', raceId, race.status);
            socket.emit('raceStatusUpdate', raceId, race.status);
          }

          const user = await userManager.getUserById(userId);
          switch (raceMember.place) {
            case 1: {
              user.balance += 20;
              user.rating += 5;
              user.wonRaceAmount += 1;
              break;
            }
            case 2: {
              user.balance += 15;
              user.rating += 4;
              break;
            }
            case 3: {
              user.balance += 10;
              user.rating += 3;
              break;
            }
            default: {
              user.balance += 5;
              user.rating += 2;
              break;
            }
          }
          user.totalRaceAmount += 1;
          user.avgCPM = Math.round((user.avgCPM + raceMember.CPM) / (user.totalRaceAmount === 1 ? 1 : 2));

          await userManager.updateUser(userId, user);
          socket.emit('userUpdate', user);
        }
        raceMember.typedLength = typedLength;
        const diffInMinutes = (new Date().valueOf() - new Date(race.date).valueOf()) / 1000 / 60;
        raceMember.CPM = Math.round(typedLength / diffInMinutes);

        await raceManager.updateRace(raceId, race);

        socket.broadcast.to(raceId).emit('raceProgressUpdate', raceId, raceMember);
        socket.emit('raceProgressUpdate', raceId, raceMember);
      }
    })
  }
];

export let userManager: UserManager;
export let raceManager: RaceManager;
export let carManager: CarManager;
export let socketsManager: SocketsManager;

export const configureManagers = async (httpServer: http.Server) => {
  const dbConnectionManager = new DBConnectionManager(process.env.DB_NAME, process.env.DB_CONNECTION_STRING);
  const db = await dbConnectionManager.connect();
  const users = await db.collection('users');
  const races = await db.collection('races');
  const articles = await db.collection('articles');
  const cars = await db.collection('cars');

  const collectionManager = new CollectionManager({
    users,
    races,
    articles,
    cars
  })

  userManager = new UserManager(collectionManager);
  raceManager = new RaceManager(collectionManager);
  carManager = new CarManager(collectionManager);
  socketsManager = new SocketsManager(
    httpServer,
    verifyJWTOnSocketConnection,
    socketOnConnectHandlers,
    socketEventHandlers
  );
  socketsManager.listen();
};