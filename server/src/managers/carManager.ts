import { CollectionManager } from './collectionManager';
import { ICar } from './../../../shared/interfaces/car';
import { ObjectId } from 'bson';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IPage } from '../../../shared/interfaces/page';
import { CarModel } from '../../../shared/enums/carModel';

export class CarManager {
  constructor(
    private readonly collectionManager: CollectionManager
  ) { }

  public async getCarsOnSale(userId: string, parameters: PageableListParameters): Promise<IPage<ICar>> {
    const user = await this.collectionManager.users
      .findOne({ _id: new ObjectId(userId) });

    const cursor = await this.collectionManager.cars
      .find({
        model: {
          '$nin': user.cars
        }
      });
    const total = await cursor.count();
    const entities = await cursor
      .sort(parameters.orderBy, parameters.isDesc ? -1 : 1)
      .skip((parameters.page - 1) * parameters.perPageAmount)
      .limit(parameters.perPageAmount)
      .toArray();

    return {
      entities,
      total
    };
  }

  public async buyCar(userId: string, carModel: CarModel): Promise<ICar> {
    const car: ICar = await this.collectionManager.cars
      .findOne({ model: carModel });
    const user = await this.collectionManager.users
      .findOne({ _id: new ObjectId(userId) });

    if (!car) {
      throw new Error('Car not found');
    } else if (user.cars.find(model => model === carModel)) {
      throw new Error('Car is already in garage');
    } else if (user.balance < car.price) {
      throw new Error('No money for purchase');
    } else {
      user.cars.push(carModel);
      user.balance -= car.price;
      await this.collectionManager.users.save(user);
      return car;
    }
  }
}