import { IUser } from '../../../shared/interfaces/user';
import { IRace } from './../../../shared/interfaces/race';
import { Collection } from 'mongodb';
import { IArticle } from './../../../shared/interfaces/article';
import { ICar } from './../../../shared/interfaces/car';

export class CollectionManager {
  public readonly users: Collection<IUser>;
  public readonly races: Collection<IRace>;
  public readonly articles: Collection<IArticle>;
  public readonly cars: Collection<ICar>;

  constructor(collections: { [key: string]: Collection<any> }) {
    for (let collectionName in collections) {
      this[collectionName] = collections[collectionName];
    }
  }
}