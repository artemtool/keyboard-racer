import { CollectionManager } from './collectionManager';
import { IUser } from './../../../shared/interfaces/user';
import { CarModel } from '../../../shared/enums/carModel';
import * as sha1 from 'sha1';
import { SignOptions } from 'jsonwebtoken';
import { createJWToken } from '../common/auth';
import { ObjectId } from 'mongodb';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IPage } from '../../../shared/interfaces/page';

export class UserManager {
  constructor(
    private readonly collectionManager: CollectionManager
  ) { }

  public getUserByEmail(email: string): Promise<IUser> {
    return this.collectionManager.users
      .findOne({ email });
  }

  public getUserById(id: string): Promise<IUser> {
    return this.collectionManager.users
      .findOne({ _id: new ObjectId(id) });
  }

  public createUser(
    email: string,
    password: string,
    nickname: string
  ): Promise<IUser> {
    const user: IUser = {
      _id: null,
      email: email,
      password: sha1(password),
      nickname: nickname,
      avgCPM: 0,
      totalRaceAmount: 0,
      wonRaceAmount: 0,
      role: 'User',
      cars: [
        CarModel.Chevrolet_Malibu
      ],
      balance: 0,
      rating: 0
    };

    return this.collectionManager.users
      .insertOne(user)
      .then(result => {
        user._id = result.insertedId.toHexString();
        return user;
      });
  }

  public createAccessToken(user: IUser): string {
    const now = new Date().valueOf();
    const day = 1000 * 60 * 60 * 24;
    const jwtOptions: SignOptions = {
      algorithm: 'HS256',
      expiresIn: now + day,
      notBefore: now
    };
    const jwtPayload = {
      userId: user._id,
      role: user.role
    };

    return createJWToken(jwtOptions, jwtPayload);
  }

  public checkUserPassword(user: IUser, password: string): boolean {
    const hashFromDb = user.password;
    const hashFromReq = sha1(password);
    return hashFromDb === hashFromReq;
  }

  public async getGlobalRating(parameters: PageableListParameters): Promise<IPage<IUser>> {
    const cursor = await this.collectionManager.users
      .find();
    const total = await cursor.count();
    const entities = await cursor
      .sort(parameters.orderBy, parameters.isDesc ? -1 : 1)
      .skip((parameters.page - 1) * parameters.perPageAmount)
      .limit(parameters.perPageAmount)
      .toArray();

    return {
      total,
      entities
    };
  }

  public async updateUser(userId: string, user: IUser): Promise<void> {
    await this.collectionManager.users.save(user);
  }
}